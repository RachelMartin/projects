﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FileMover
{
    public class FileMover
    {
        public enum DestinationFormats
        {
            Year_Month_Day,
            Year
        }

        public static void MovePictures(string fromLoc, string toLoc, DateTime firstDate = new DateTime())
        {
            Move(fromLoc, toLoc, DestinationFormats.Year_Month_Day, new List<string>() { ".jpg", ".jpeg" }, firstDate);
        }

        public static void MoveVideos(string fromLoc, string toLoc, DateTime firstDate = new DateTime())
        {
            Move(fromLoc, toLoc, DestinationFormats.Year, new List<string>() { ".avi", ".mp4" }, firstDate);
        }

        public static void Move(string fromLoc, string toLoc,
            DestinationFormats format = DestinationFormats.Year_Month_Day,
            List<string> fileTypes = null,
            DateTime firstDate = new DateTime())
        {
            if (fileTypes == null)
                // Add jpg to the list
                fileTypes = new List<string>() { ".jpg", ".jpeg" };

            // Get the files
            DirectoryInfo diFrom = new DirectoryInfo(fromLoc);
            IEnumerable<FileInfo> fiFromFiles = diFrom.EnumerateFiles()
                .Where(file => fileTypes.Any(type => file.Extension.Equals(type, StringComparison.OrdinalIgnoreCase)))
                .Where(file => file.LastWriteTime > firstDate);

            // Write to the directory
            DirectoryInfo diTo = new DirectoryInfo(toLoc);
            foreach (FileInfo fiFrom in fiFromFiles)
            {
                string toDir = BuildDestinationPath(toLoc, fiFrom, format);
                Directory.CreateDirectory(toDir);
                try
                {
                    File.Copy(fiFrom.FullName, toDir + "\\" + fiFrom.Name, false);
                }
                catch(IOException)
                {
                    // File already exists
                }
            }
        }

        private static string BuildDestinationPath(string toLoc, FileInfo fiFrom, DestinationFormats format)
        {
            string toDir = string.Empty;
            switch (format)
            {
                case DestinationFormats.Year:
                    toDir = string.Format("{0}\\{1}",
                        toLoc,
                        fiFrom.LastWriteTime.Year);
                    break;
                case DestinationFormats.Year_Month_Day:
                default:
                    toDir = string.Format("{0}\\{1}\\{2}{3}\\{1}_{2}{3}_{4}{5}",
                        toLoc,
                        fiFrom.LastWriteTime.Year,
                        fiFrom.LastWriteTime.Month < 10 ? "0" : string.Empty,
                        fiFrom.LastWriteTime.Month,
                        fiFrom.LastWriteTime.Day < 10 ? "0" : string.Empty,
                        fiFrom.LastWriteTime.Day);
                    break;
            }
            return toDir;
        }
    }
}
