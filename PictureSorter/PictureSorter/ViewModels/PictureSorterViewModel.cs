﻿//using Microsoft.TeamFoundation.MVVM;
using System;
using System.ComponentModel;
using System.Windows.Forms;

namespace PictureSorter.ViewModels
{
	public class PictureSorterViewModel : INotifyPropertyChanged
    {
        public PictureSorterViewModel()
        {
        }
        private FolderBrowserDialog _browseDialog = new FolderBrowserDialog();

        #region Properties
        public bool MoveAll
        {
            get { return Properties.Settings.Default.MoveAll; }
            set
            {
                if(Properties.Settings.Default.MoveAll != value)
                {
                    Properties.Settings.Default.MoveAll = value;
                    Properties.Settings.Default.Save();
                    OnPropertyChanged("MoveAll");
                }
            }
        }

        public string SourceFolder
        {
            get { return Properties.Settings.Default.SourceFolder; }
            set
            {
                if(Properties.Settings.Default.SourceFolder != value)
                {
                    Properties.Settings.Default.SourceFolder = value;
                    Properties.Settings.Default.Save();
                    OnPropertyChanged("SourceFolder");
                }
            }
        }

        public string DestinationFolder
        {
            get { return Properties.Settings.Default.DestinationFolder; }
            set
            {
                if (Properties.Settings.Default.DestinationFolder != value)
                {
                    Properties.Settings.Default.DestinationFolder = value;
                    Properties.Settings.Default.Save();
                    OnPropertyChanged("DestinationFolder");
                }
            }
        }

        public DateTime FirstDate
        {
            get { return Properties.Settings.Default.FirstDate; }
            set
            {
                if(Properties.Settings.Default.FirstDate != value)
                {
                    Properties.Settings.Default.FirstDate = value;
                    Properties.Settings.Default.Save();
                    OnPropertyChanged("FirstDate");
                }
            }
        }

		public bool MovePictures
		{
			get { return Properties.Settings.Default.MovePictures; }
			set
			{
				if (Properties.Settings.Default.MovePictures != value)
				{
					Properties.Settings.Default.MovePictures = value;
					Properties.Settings.Default.Save();
					OnPropertyChanged("MovePictures");
				}
			}
		}

		public bool MoveVideos {
			get { return Properties.Settings.Default.MoveVideos; }
			set
			{
				if(Properties.Settings.Default.MoveVideos != value)
				{
					Properties.Settings.Default.MoveVideos = value;
					Properties.Settings.Default.Save();
					OnPropertyChanged("MoveVideos");
				}
			}
		}
		#endregion

		#region Commands
		private RelayCommand _browseFolderCommand;
        public RelayCommand BrowseFolderCommand
        {
            get
            {
                if(_browseFolderCommand == null)
                {
                    _browseFolderCommand = new RelayCommand(param => BrowseFolder(param));
                }
                return _browseFolderCommand;
            }
        }

        private void BrowseFolder(object param)
        {
            string location = param.ToString();

            if(location.Equals("Source", StringComparison.OrdinalIgnoreCase))
                _browseDialog.SelectedPath = SourceFolder;
            else
                _browseDialog.SelectedPath = DestinationFolder;

            if(_browseDialog.ShowDialog() == DialogResult.OK)
            {
                if (location.Equals("Source", StringComparison.OrdinalIgnoreCase))
                    SourceFolder = _browseDialog.SelectedPath;
                else
                    DestinationFolder = _browseDialog.SelectedPath;
            }
        }

        private RelayCommand _moveCommand;
        public RelayCommand MoveCommand
        {
            get
            {
                if(_moveCommand == null)
                {
                    _moveCommand = new RelayCommand(param => Move());
                }
                return _moveCommand;
            }
        }

        private void Move()
        {
            DateTime moveDate = new DateTime();
            if (!MoveAll)
                moveDate = FirstDate;
			if(MovePictures)
	            FileMover.FileMover.MovePictures(SourceFolder, DestinationFolder, moveDate);
			else
				FileMover.FileMover.MoveVideos(SourceFolder, DestinationFolder, moveDate);
		}

        #endregion

        #region INotifyPropertyChanged
        public event PropertyChangedEventHandler PropertyChanged;

        private void OnPropertyChanged(string property)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(property));
        }
        #endregion
    }
}
