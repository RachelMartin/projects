﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HealthBars
{
	public class Character : ViewModelBase
	{
		#region Members
		private string _characterName;
		private string _playerName;
		private int _maxHP;
		private int _currentHP;
		private int _temporaryHP;
		#endregion

		#region Properties
		public string CharacterName
		{
			get { return _characterName; }
			set { _characterName = value;  NotifyPropertyChanged("CharacterName"); }
		}
		public string PlayerName
		{
			get { return _playerName; }
			set { _playerName = value; NotifyPropertyChanged("PlayerName"); }
		}
		public int MaxHP
		{
			get { return _maxHP; }
			set {
				_maxHP = value;  NotifyPropertyChanged("MaxHP");
				NotifyPropertyChanged("HalfHP");
				NotifyPropertyChanged("MissingHP");
				NotifyPropertyChanged("ToHalfHP");
			}
		}
		public int CurrentHP
		{
			get { return _currentHP; }
			set {
				_currentHP = value;
				NotifyPropertyChanged("CurrentHP");
				NotifyPropertyChanged("MissingHP");
				NotifyPropertyChanged("ToHalfHP");
			}
		}
		public int TemporaryHP
		{
			get { return _temporaryHP; }
			set { _temporaryHP = value; NotifyPropertyChanged("TemporaryHP"); }
		}
		public int HalfHP
		{
			get { return MaxHP / 2; }
		}
		public int MissingHP
		{
			get { return MaxHP > CurrentHP ? MaxHP - CurrentHP : 0; }
		}
		public int ToHalfHP
		{
			get { return HalfHP > CurrentHP ? HalfHP - CurrentHP : 0; }
		}
		#endregion

		#region Constructors
		public Character()
		{}

		public Character(string characterName, string playerName, int maxHP)
		{
			CharacterName = characterName;
			PlayerName = playerName;
			MaxHP = maxHP;
			CurrentHP = maxHP;
		}
		#endregion

		#region Public Methods
		public void Aid(int maxHP, int currentHP, bool add)
		{
			if(add)
			{
				MaxHP += maxHP;
				CurrentHP += currentHP;
			}
			else
			{
				MaxHP -= maxHP;
				CurrentHP -= currentHP;
			}
		}

		public void ChangeHP(int changedAmount, bool addTemp = false)
		{
			// Current HP
			if (addTemp || changedAmount < 0)
				CurrentHP += changedAmount;
			else if (changedAmount + CurrentHP > MaxHP + TemporaryHP)
				CurrentHP = MaxHP + TemporaryHP;
			else
				CurrentHP += changedAmount;

			if (CurrentHP < 0)
				CurrentHP = 0;

			// Temporary HP
			if (CurrentHP > MaxHP)
				TemporaryHP = CurrentHP - MaxHP;
			else
				TemporaryHP = 0;
		}

		public void SetHP(int newHP)
		{
			CurrentHP = newHP;
		}
		#endregion
	}
}
