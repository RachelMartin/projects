﻿using System.Windows;

namespace HealthBars
{
	public partial class Aid : Window
	{
		AidViewModel _viewModel;

		public Aid(AidViewModel viewModel)
		{
			InitializeComponent();
			_viewModel = viewModel;
			DataContext = _viewModel;
		}

		private void OK_Click(object sender, RoutedEventArgs e)
		{
			DialogResult = true;
		}
	}
}
