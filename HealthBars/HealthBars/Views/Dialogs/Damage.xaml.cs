﻿using System.Windows;

namespace HealthBars
{
	public partial class Damage : Window
	{
		HealDamageViewModel _viewModel;

		public Damage(HealDamageViewModel viewModel)
		{
			InitializeComponent();
			_viewModel = viewModel;
			DataContext = _viewModel;
		}

		private void OK_Click(object sender, RoutedEventArgs e)
		{
			DialogResult = true;
		}
	}
}
