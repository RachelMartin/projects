﻿using System.Windows;

namespace HealthBars
{
	public partial class AddCharacter : Window
	{
		HealthBarsViewModel _viewModel;

		public AddCharacter(HealthBarsViewModel viewModel)
		{
			InitializeComponent();
			_viewModel = viewModel;
			DataContext = _viewModel;
		}

		private void OK_Click(object sender, RoutedEventArgs e)
		{
			DialogResult = true;
		}
	}
}
