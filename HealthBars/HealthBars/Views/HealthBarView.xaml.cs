﻿using System.Windows.Controls;

namespace HealthBars
{
	public partial class HealthBarView : UserControl
	{
		private HealthBarViewModel viewModel;

		public HealthBarView()
		{
			InitializeComponent();
			viewModel = DataContext as HealthBarViewModel;
		}

		private void Aid_Click(object sender, System.Windows.RoutedEventArgs e)
		{
			viewModel = DataContext as HealthBarViewModel;
			AidViewModel vm = new AidViewModel();
			Aid aid = new Aid(vm);
			if (aid.ShowDialog() == true)
				viewModel.Aid(vm.MaxHP, vm.CurrentHP, vm.Add);
		}

		private void Damage_Click(object sender, System.Windows.RoutedEventArgs e)
		{
			viewModel = DataContext as HealthBarViewModel;
			HealDamageViewModel vm = new HealDamageViewModel();
			Damage heal = new Damage(vm);
			if (heal.ShowDialog() == true)
				viewModel.Damage(vm.Amount);
		}

		private void Heal_Click(object sender, System.Windows.RoutedEventArgs e)
		{
			viewModel = DataContext as HealthBarViewModel;
			HealDamageViewModel vm = new HealDamageViewModel();
			Heal heal = new Heal(vm);
			if (heal.ShowDialog() == true)
				viewModel.Heal(vm.Amount, vm.Temp);
		}
	}
}
