﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Collections.ObjectModel;
using System.Text;
using System.Threading.Tasks;

namespace HealthBars
{
	public class HealthBarsViewModel : ViewModelBase
	{
		#region Members
		private string _characterName;
		private int _maxHP;
		private string _playerName;
		#endregion

		#region Properties
		public string CharacterName
		{
			get { return _characterName; }
			set { _characterName = value; NotifyPropertyChanged("CharacterName"); }
		}
		public int MaxHP
		{
			get { return _maxHP; }
			set { _maxHP = value; NotifyPropertyChanged("MaxHP"); }
		}
		public string PlayerName
		{
			get { return _playerName; }
			set { _playerName = value; NotifyPropertyChanged("PlayerName"); }
		}
		public ObservableCollection<HealthBarViewModel> HealthBars { get; set; }
		#endregion

		#region Constructor
		public HealthBarsViewModel()
		{
			HealthBars = new ObservableCollection<HealthBarViewModel>();
		}
		#endregion

		#region Methods
		public void AddCharacter()
		{
			HealthBars.Add(new HealthBarViewModel(CharacterName, PlayerName, MaxHP));
		}

		public void DamageAll(int amount)
		{
			foreach (HealthBarViewModel character in HealthBars)
			{
				character.Damage(amount);
			}
		}

		public void HealAll(int amount, bool temp)
		{
			foreach (HealthBarViewModel character in HealthBars)
			{
				character.Heal(amount, temp);
			}
		}

		public void RemoveCharacter(HealthBarViewModel character)
		{
			HealthBars.Remove(character);
		}
		#endregion
	}
}
