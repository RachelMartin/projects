﻿namespace HealthBars
{
	public class AidViewModel : ViewModelBase
	{
		#region Members
		private bool _add;
		private int _currentHP;
		private int _maxHP;
		#endregion

		#region Properties
		public bool Add
		{
			get { return _add; }
			set { _add = value;  NotifyPropertyChanged("Add"); }
		}

		public int CurrentHP
		{
			get { return _currentHP; }
			set { _currentHP = value; NotifyPropertyChanged("CurrentHP"); }
		}

		public int MaxHP
		{
			get { return _maxHP; }
			set { _maxHP = value; NotifyPropertyChanged("MaxHP"); }
		}
		#endregion
	}
}
