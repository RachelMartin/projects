﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HealthBars
{
	public class HealthBarViewModel : ViewModelBase
	{
		#region Members
		private Character _character;
		#endregion

		#region Properties
		public Character Character
		{
			get { return _character; }
			set { _character = value;  NotifyPropertyChanged("Character"); }
		}
		#endregion

		#region Constructor
		public HealthBarViewModel()
		{
			Character = new Character();
		}

		public HealthBarViewModel(string characterName, string playerName, int maxHP)
		{
			Character = new Character(characterName, playerName, maxHP);
		}
		#endregion

		#region Methods
		public void Aid(int maxHP, int currentHP, bool add)
		{
			Character.Aid(maxHP, currentHP, add);
		}

		public void Damage(int amount)
		{
			Character.ChangeHP(0 - amount);
		}

		public void Heal(int amount, bool temp)
		{
			Character.ChangeHP(amount, temp);
		}

		public void SetHP(int amount)
		{
			Character.SetHP(amount);
		}
		#endregion
	}
}
