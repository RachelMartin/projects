﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HealthBars
{
	public class HealDamageViewModel : ViewModelBase
	{
		#region Members
		private int _amount;
		private bool _temp;
		#endregion

		#region Properties
		public int Amount
		{
			get { return _amount; }
			set { _amount = value; NotifyPropertyChanged("Amount"); }
		}
		public bool Temp
		{
			get { return _temp; }
			set { _temp = value; NotifyPropertyChanged("Temp"); }
		}
		#endregion
	}
}
