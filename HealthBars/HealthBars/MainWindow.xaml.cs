﻿using System.Windows;

namespace HealthBars
{
	public partial class MainWindow : Window
	{
		private HealthBarsViewModel viewModel;

		public MainWindow()
		{
			InitializeComponent();
			viewModel = new HealthBarsViewModel();
			DataContext = viewModel;
		}

		private void Add_Click(object sender, RoutedEventArgs e)
		{
			AddCharacter add = new AddCharacter(viewModel);
			if (add.ShowDialog() == true)
				viewModel.AddCharacter();
		}

		private void DamageAll_Click(object sender, RoutedEventArgs e)
		{
			HealDamageViewModel vm = new HealDamageViewModel();
			Damage damage = new Damage(vm);
			if (damage.ShowDialog() == true)
				viewModel.DamageAll(vm.Amount);
		}

		private void HealAll_Click(object sender, RoutedEventArgs e)
		{
			HealDamageViewModel vm = new HealDamageViewModel();
			Heal heal = new Heal(vm);
			if (heal.ShowDialog() == true)
				viewModel.HealAll(vm.Amount, vm.Temp);
		}
	}
}
