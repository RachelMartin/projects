﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using HealthBars;

namespace HealthBars.Test
{
	[TestClass]
	public class CharacterTest
	{
		private Character adam;

		#region Setup
		[TestInitialize]
		public void Initialize()
		{
			adam = new Character("Adam", "Adam", 20);
		}
		#endregion

		#region Aid
		[TestMethod]
		public void Aid_Add()
		{
			adam.Aid(5, 5, true);
			Assert.AreEqual(25, adam.CurrentHP);
			Assert.AreEqual(25, adam.MaxHP);
			Assert.AreEqual(12, adam.HalfHP);
			Assert.AreEqual(0, adam.TemporaryHP);
		}

		[TestMethod]
		public void Aid_WithTemp()
		{
			adam.ChangeHP(2, true);
			adam.Aid(5, 5, true);
			Assert.AreEqual(27, adam.CurrentHP);
			Assert.AreEqual(25, adam.MaxHP);
			Assert.AreEqual(12, adam.HalfHP);
			Assert.AreEqual(2, adam.TemporaryHP);
		}

		[TestMethod]
		public void Aid_Remove()
		{
			adam.Aid(5, 5, false);
			Assert.AreEqual(15, adam.CurrentHP);
			Assert.AreEqual(15, adam.MaxHP);
			Assert.AreEqual(7, adam.HalfHP);
			Assert.AreEqual(0, adam.TemporaryHP);
		}

		[TestMethod]
		public void Aid_RemoveWithTemp()
		{
			adam.ChangeHP(2, true);
			adam.Aid(5, 5, false);
			Assert.AreEqual(17, adam.CurrentHP);
			Assert.AreEqual(15, adam.MaxHP);
			Assert.AreEqual(7, adam.HalfHP);
			Assert.AreEqual(2, adam.TemporaryHP);
		}
		#endregion

		#region HalfHP
		[TestMethod]
		public void HalfHp_WhenEven()
		{
			Assert.AreEqual(10, adam.HalfHP);
		}

		[TestMethod]
		public void HalfHp_WhenOdd()
		{
			adam.MaxHP = 19;
			Assert.AreEqual(9, adam.HalfHP);
		}
		#endregion

		#region ChangeHP
		[TestMethod]
		public void ChangeHP_WhenAdd()
		{
			adam.CurrentHP = 10;
			adam.ChangeHP(1);
			Assert.AreEqual(11, adam.CurrentHP);
		}

		[TestMethod]
		public void ChangeHP_WhenAddTemp()
		{
			adam.ChangeHP(5, true);
			Assert.AreEqual(25, adam.CurrentHP);
			Assert.AreEqual(20, adam.MaxHP);
			Assert.AreEqual(5, adam.TemporaryHP);
		}

		[TestMethod]
		public void ChangeHP_WhenAddPartialTemp()
		{
			adam.SetHP(18);
			adam.ChangeHP(5, true);
			Assert.AreEqual(23, adam.CurrentHP);
			Assert.AreEqual(20, adam.MaxHP);
			Assert.AreEqual(3, adam.TemporaryHP);
		}

		[TestMethod]
		public void ChangeHP_WhenAddNoTemp()
		{
			adam.ChangeHP(5);
			Assert.AreEqual(20, adam.CurrentHP);
			Assert.AreEqual(20, adam.MaxHP);
			Assert.AreEqual(0, adam.TemporaryHP);
		}

		[TestMethod]
		public void ChangeHP_WhenAddDontResetTemp()
		{
			adam.ChangeHP(5, true);
			Assert.AreEqual(25, adam.CurrentHP);
			adam.ChangeHP(1);
			Assert.AreEqual(25, adam.CurrentHP);
		}

		[TestMethod]
		public void ChangeHP_WhenSubtract()
		{
			adam.ChangeHP(-1);
			Assert.AreEqual(19, adam.CurrentHP);
		}

		[TestMethod]
		public void ChangeHP_WhenSubtractStopAtZero()
		{
			adam.ChangeHP(-30);
			Assert.AreEqual(0, adam.CurrentHP);
		}

		[TestMethod]
		public void ChangeHP_WhenSubtractRemoveTemp()
		{
			adam.ChangeHP(5, true);
			Assert.AreEqual(25, adam.CurrentHP);
			Assert.AreEqual(20, adam.MaxHP);
			Assert.AreEqual(5, adam.TemporaryHP);

			adam.ChangeHP(-5);
			Assert.AreEqual(20, adam.CurrentHP);
			Assert.AreEqual(20, adam.MaxHP);
			Assert.AreEqual(0, adam.TemporaryHP);
		}

		[TestMethod]
		public void ChangeHP_WhenSubtractRemoveFromTempOnly()
		{
			adam.ChangeHP(5, true);
			Assert.AreEqual(25, adam.CurrentHP);
			Assert.AreEqual(20, adam.MaxHP);
			Assert.AreEqual(5, adam.TemporaryHP);

			adam.ChangeHP(-2);
			Assert.AreEqual(23, adam.CurrentHP);
			Assert.AreEqual(20, adam.MaxHP);
			Assert.AreEqual(3, adam.TemporaryHP);
		}
		#endregion

		#region SetHP
		[TestMethod]
		public void SetHP()
		{
			adam.SetHP(10);
			Assert.AreEqual(10, adam.CurrentHP);
		}
		#endregion
	}
}
